module go.bfi.io/reuzi/flador

go 1.13

require (
	github.com/h2non/filetype v1.1.0
	github.com/nwaples/rardecode v1.1.0
	github.com/stretchr/testify v1.6.1
	github.com/ulikunitz/xz v0.5.8
)
