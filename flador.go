// Package flador is general purpose extractor of various archive
// formats. Name is contraction of Portuguese 'inflador', meaning
// 'inflator'.
package flador

import (
	"archive/tar"
	"archive/zip"
	"bytes"
	"compress/bzip2"
	"compress/gzip"
	"crypto/sha256"
	"fmt"
	"io"
	"net/http"
	"os"
	"path"
	"strings"

	"github.com/h2non/filetype"
	"github.com/nwaples/rardecode"
	"github.com/ulikunitz/xz"
)

// Version holds release version number. Modified on compile time
// when building executables.
var Version = "devel"

// Revision holds release revision number. Modified on compile time
// when building executables.
var Revision = "00000000-0000000"

// Flador struct.
type Flador struct {
	// Fname is the source file.
	Fname string
	// Exts is suffixes for filtering.
	Exts []string
	// Dir is destination directory.
	Dir string

	extractOneWriter io.Writer
	extractOneDone   bool
}

// NewFlador initializes Flador struct.
func NewFlador(fname string, exts []string, dir string) *Flador {
	return &Flador{
		Fname: fname,
		Exts:  exts,
		Dir:   dir,
	}
}

// SmallReader opens just about enough bytes for detecting magic number.
// NOTE: http.DetectContentType needs 256, however this apparently
// causes failure on tar files if not set to 1024.
func (d Flador) SmallReader(fl string) ([]byte, error) {
	buff := make([]byte, 1024)
	hn, err := os.Open(fl)
	if err != nil {
		return nil, err
	}
	if stt, _ := hn.Stat(); stt.IsDir() {
		return nil, fmt.Errorf("cannot read directory")
	}
	hn.Read(buff)
	return buff, nil
}

//  GetMime tries to detect MIME types.
func (d Flador) GetMime(buff []byte) string {
	ft, unknown := filetype.Match(buff)
	if unknown != nil || ft.MIME.Value == "" {
		return http.DetectContentType(buff)
	}
	return ft.MIME.Value
}

// GetMimeFromReader attempts to detect MIME type from a file reader.
// NOTE: This reads the entire file.
func (d Flador) GetMimeFromReader(r io.Reader) string {
	buff := bytes.NewBuffer(nil)
	io.Copy(buff, r)
	return d.GetMime([]byte(buff.Bytes()))
}

// matchExtension is used to select files within the archive based
// on extension alone. The rest that don't match are ignored.
func (d *Flador) matchExtension(name string) bool {
	if len(d.Exts) < 1 {
		return true
	}
	for _, ext := range d.Exts {
		name := strings.ToLower(name)
		if strings.HasSuffix(name, ext) {
			return true
		}
	}
	return false
}

// GetHash generates SHA256 hash string of a buffer, useful for renaming
// file based on its content. Slice the buffer if it's too big.
func (d Flador) GetHash(buff []byte) string {
	hash := sha256.New()
	hash.Write(buff)
	return fmt.Sprintf("%x", hash.Sum(nil))
}

func (d *Flador) writeFile(leaf string, r io.Reader) error {
	if d.extractOneWriter != nil {
		io.Copy(d.extractOneWriter, r)
		d.extractOneDone = true
		return nil
	}

	pfile := path.Join(d.Dir, leaf)
	pdir := path.Dir(pfile)
	if err := os.MkdirAll(pdir, 0755); err != nil {
		return err
	}
	hn, err := os.OpenFile(
		pfile, os.O_WRONLY|os.O_CREATE|os.O_EXCL, 0600)
	if err != nil {
		return err
	}
	defer hn.Close()
	io.Copy(hn, r)
	return nil
}

// Untar extracts all files in a tar blob.
func (d *Flador) Untar(r io.Reader) error {
	tr := tar.NewReader(r)
	i := 0
	for {
		hdr, err := tr.Next()
		if err != nil {
			if err == io.EOF {
				break
			}
			return fmt.Errorf("broken archive")
		}
		if !d.matchExtension(hdr.Name) {
			continue
		}
		if hdr.Typeflag == tar.TypeReg {
			if err := d.writeFile(hdr.Name, tr); err != nil {
				return err
			}
			i++
			if d.extractOneDone {
				return nil
			}
		}
	}

	if i < 1 {
		return fmt.Errorf("no files extracted")
	}
	return nil
}

// Unzip decompresses ZIP file.
func (d *Flador) Unzip() error {
	zr, _ := zip.OpenReader(d.Fname)
	defer zr.Close()
	i := 0
	for _, zfl := range zr.File {
		zhn, _ := zfl.Open()
		defer zhn.Close()

		// skip directory
		if zfl.FileInfo().IsDir() {
			// FIXME: This effectively skips directory
			// without content.
			continue
		}

		if !d.matchExtension(zfl.Name) {
			continue
		}
		if err := d.writeFile(zfl.Name, zhn); err != nil {
			return err
		}
		i++
		if d.extractOneDone {
			return nil
		}
	}

	if i < 1 {
		return fmt.Errorf("no files extracted")
	}
	return nil
}

// Gunzip archive.
func (d *Flador) Gunzip() error {
	fname := d.Fname

	arx, err := os.Open(fname)
	if err != nil {
		return err
	}
	defer arx.Close()

	rgz, err := gzip.NewReader(arx)
	if err != nil {
		return err
	}
	defer rgz.Close()

	name := rgz.Header.Name
	mime := d.GetMimeFromReader(rgz)
	rgz.Close()

	// must reset io.Reader
	arx.Seek(0, 0)
	rgz, _ = gzip.NewReader(arx)
	defer rgz.Close()

	if mime == "application/x-tar" {
		return d.Untar(rgz)
	}

	if !d.matchExtension(name) {
		return fmt.Errorf("no files extracted")
	}

	return d.writeFile(name, rgz)
}

// Bunzip2 archive.
func (d *Flador) Bunzip2() error {
	fname := d.Fname

	arx, err := os.Open(fname)
	if err != nil {
		return err
	}
	defer arx.Close()

	rbz := bzip2.NewReader(arx)
	mime := d.GetMimeFromReader(rbz)

	// must reset io.Reader
	arx.Seek(0, 0)
	rbz = bzip2.NewReader(arx)

	if mime == "application/x-tar" {
		return d.Untar(rbz)
	}

	// bzip2 doesn't have dedicated header
	name := strings.TrimSuffix(path.Base(fname), ".bz2")

	if !d.matchExtension(name) {
		return fmt.Errorf("no files extracted")
	}

	return d.writeFile(name, rbz)
}

// UnXz archive, similar to `xz -d`.
func (d *Flador) UnXz() error {
	fname := d.Fname

	arx, err := os.Open(fname)
	if err != nil {
		return err
	}
	defer arx.Close()

	rxz, err := xz.NewReader(arx)
	if err != nil {
		return err
	}
	mime := d.GetMimeFromReader(rxz)

	// must reset io.Reader
	arx.Seek(0, 0)
	rxz, _ = xz.NewReader(arx)

	if mime == "application/x-tar" {
		return d.Untar(rxz)
	}

	// xz doesn't have dedicated header
	name := strings.TrimSuffix(path.Base(fname), ".xz")

	if !d.matchExtension(name) {
		return fmt.Errorf("no files extracted")
	}

	return d.writeFile(name, rxz)
}

// Unrar a RAR archive.
func (d *Flador) Unrar() error {
	fname := d.Fname

	arx, err := os.Open(fname)
	if err != nil {
		return err
	}
	defer arx.Close()

	rar, err := rardecode.NewReader(arx, "")
	if err != nil {
		return err
	}

	i := 0
	for {
		hdr, err := rar.Next()
		if err != nil {
			// rar doesn't detect EOF
			break
		}
		if hdr.IsDir {
			continue
		}
		if !d.matchExtension(hdr.Name) {
			continue
		}

		if err := d.writeFile(hdr.Name, rar); err != nil {
			return err
		}
		i++
		if d.extractOneDone {
			return nil
		}
	}

	if i < 1 {
		return fmt.Errorf("no files extracted")
	}
	return nil
}

// Extract an archive.
func (d *Flador) Extract() error {
	fname := d.Fname

	splice, err := d.SmallReader(fname)
	if err != nil {
		return err
	}

	switch d.GetMime(splice) {
	case "application/x-tar":
		r, _ := os.Open(fname)
		defer r.Close()
		return d.Untar(r)
	case "application/zip":
		return d.Unzip()
	case "application/gzip":
		return d.Gunzip()
	case "application/x-bzip2":
		return d.Bunzip2()
	case "application/x-xz":
		return d.UnXz()
	case "application/x-7z-compressed":
		return fmt.Errorf("TODO")
	case "application/x-rar-compressed":
		fallthrough
	case "application/vnd.rar":
		return d.Unrar()
	}

	return fmt.Errorf("archive format not supported")
}

// ExtractOne extracts exactly one file.
func (d *Flador) ExtractOne(out io.Writer) error {
	d.extractOneWriter = out
	return d.Extract()
}

// ExtractOneToFile extracts exactly one file and writes it to a
// newly-created file.
func (d *Flador) ExtractOneToFile(fname string) error {
	hn, err := os.OpenFile(
		fname, os.O_WRONLY|os.O_CREATE|os.O_EXCL, 0600)
	if err != nil {
		return err
	}
	err = d.ExtractOne(hn)
	if err != nil {
		hn.Close()
		os.Remove(fname)
		return err
	}
	hn.Close()
	return nil
}
