
VTAG  := $(shell \
	git describe --abbrev=0 2>/dev/null | \
	sed 's:^v::' \
)
ifeq ($(VTAG),)
	VTAG := devel
endif

VREV  := $(shell \
	git log --pretty=format:'%ad-%h' -n1 --date=format:%Y%m%d \
)

COVDIR := docs/coverage

PKG := "go.bfi.io/reuzi/flador"

GOOS ?= linux
GOARCH ?= amd64

BIN := flador
ifeq ($(GOOS),windows)
	BIN := flador.exe
endif

all: dist

deps:
	go get -v

dist:
	go build \
		-gcflags "all='-trimpath=$(shell pwd)'" \
		-ldflags " \
			-s -w \
			-X $(PKG).Version=$(VTAG) \
			-X $(PKG).Revision=$(VREV) \
		" ./cmd/flador

dist-upx: dist
	upx --brute $(BIN)

dist-gccgo:
	@# gccgo, dynamically linked to glibc and libgo, no -X flag support
	@# usage: docker-gcctest '-r dist-gccgo' -u 9
	go build -o stubgo-gccgo -compiler gccgo \
		-gccgoflags " \
			-s -w \
		" ./cmd/flador

lint:
	bfi-revive ./...

check: tests

tests: lint
	@mkdir -p $(COVDIR)
	@go test -test.v -coverprofile $(COVDIR)/coverage.out ./...
	@go tool cover -html $(COVDIR)/coverage.out -o \
		$(COVDIR)/index.html
	@goclover -f $(COVDIR)/coverage.out -o $(COVDIR)/coverage.xml

clean:
	git clean -xdf

.PHONY: dist
