package flador

import (
	"io/ioutil"
	"os"
	"path"
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestReader(t *testing.T) {
	dir := "testdata"

	// cannot read directory
	_, err := (Flador{}).SmallReader(dir)
	assert.NotNil(t, err)

	// file not found
	_, err = (Flador{}).SmallReader(path.Join(dir, "none"))
	assert.NotNil(t, err)

	// read ok
	fnameZip := path.Join(dir, "bar.zip")
	buff, _ := (Flador{}).SmallReader(fnameZip)
	assert.Equal(t, "PK", string(buff)[0:2])

	// mime from byte buffer ok
	buff, _ = ioutil.ReadFile(fnameZip)
	mime := (Flador{}).GetMime(buff)
	assert.Equal(t, "application/zip", mime)

	// mime from io.Reader ok
	hn, _ := os.Open(fnameZip)
	mime = (Flador{}).GetMimeFromReader(hn)
	assert.Equal(t, "application/zip", mime)

	hash := (Flador{}).GetHash(buff)
	assert.Equal(t, "97e8c3e23", hash[0:9])
}

func setUp(base ...string) *Flador {
	leaves := append([]string{"testdata"}, base...)
	fname := path.Join(leaves...)
	tdir, _ := ioutil.TempDir("", "flador-test-*")
	return NewFlador(fname, nil, tdir)
}

func tearDown(fld *Flador) {
	os.RemoveAll(fld.Dir)
}

func checkFormat(t *testing.T, fnameWithDir, fnameSingle string) {
	// with dir
	fld := setUp(fnameWithDir)
	defer tearDown(fld)
	err := fld.Extract()
	assert.Nil(t, err)
	data, _ := ioutil.ReadFile(path.Join(fld.Dir, "foo", "bar"))
	assert.Equal(t, "baz\n", string(data))

	// with dir, filtered
	fld = setUp(fnameWithDir)
	defer tearDown(fld)
	fld.Exts = []string{".txt"}
	err = fld.Extract()
	assert.Nil(t, err)
	data, err = ioutil.ReadFile(path.Join(fld.Dir, "foo", "quux.txt"))
	assert.Nil(t, err)
	assert.Equal(t, "baz\n", string(data))
	_, err = ioutil.ReadFile(path.Join(fld.Dir, "foo", "bar"))
	assert.NotNil(t, err)

	// with dir, filtered, no match
	fld = setUp(fnameWithDir)
	defer tearDown(fld)
	fld.Exts = []string{".docx"}
	err = fld.Extract()
	assert.NotNil(t, err)

	// with dir, extract one
	fld = setUp(fnameWithDir)
	defer tearDown(fld)
	dst := path.Join(fld.Dir, "result")
	err = fld.ExtractOneToFile(dst)
	assert.Nil(t, err)
	sdata, _ := ioutil.ReadFile(dst)
	assert.Equal(t, data, sdata)

	// single file
	fld = setUp(fnameSingle)
	defer tearDown(fld)
	err = fld.Extract()
	assert.Nil(t, err)
	data, _ = ioutil.ReadFile(path.Join(fld.Dir, "bar.txt"))
	assert.Equal(t, "baz\n", string(data))

	// single file, filtered
	fld = setUp(fnameSingle)
	defer tearDown(fld)
	fld.Exts = []string{".txt"}
	err = fld.Extract()
	assert.Nil(t, err)
	data, _ = ioutil.ReadFile(path.Join(fld.Dir, "bar.txt"))
	assert.Equal(t, "baz\n", string(data))

	// single file, filtered, no match
	fld = setUp(fnameSingle)
	defer tearDown(fld)
	fld.Exts = []string{".docx"}
	err = fld.Extract()
	assert.NotNil(t, err)

	// single file, extract one
	fld = setUp(fnameSingle)
	defer tearDown(fld)
	dst = path.Join(fld.Dir, "result")
	err = fld.ExtractOneToFile(dst)
	assert.Nil(t, err)
	sdata, _ = ioutil.ReadFile(dst)
	assert.Equal(t, data, sdata)

	// single file, extract one, destination exists
	dst = path.Join(fld.Dir, "result")
	err = fld.ExtractOneToFile(dst)
	assert.NotNil(t, err)
}

func TestUntar(t *testing.T) {
	checkFormat(t, "foo.tar", "bar.tar")

	// file not found
	fld := setUp("bar.1.tar")
	defer tearDown(fld)
	err := fld.Extract()
	assert.NotNil(t, err)

	// format unknown
	fld = setUp("..", "flador.go")
	defer tearDown(fld)
	err = fld.Extract()
	assert.NotNil(t, err)
}

func TestGunzip(t *testing.T) {
	checkFormat(t, "foo.tar.gz", "bar.txt.gz")
}

func TestBunzip2(t *testing.T) {
	checkFormat(t, "foo.tar.bz2", "bar.txt.bz2")
}

func TestUnXz(t *testing.T) {
	checkFormat(t, "foo.tar.xz", "bar.txt.xz")
}

func TestUnzip(t *testing.T) {
	checkFormat(t, "foo.zip", "bar.zip")
}

func TestUnrar(t *testing.T) {
	checkFormat(t, "foo.rar", "bar.rar")
}
