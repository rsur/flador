// Command flador is the CLI program for package flador.
package main

import (
	"flag"
	"fmt"
	"os"
	"strings"

	fld "go.bfi.io/reuzi/flador"
)

// cmdVersion prints version and revision.
func cmdVersion() {
	fmt.Printf(`
Flador
  Version:  %s
  Revision: %s

`, fld.Version, fld.Revision)
	os.Exit(0)
}

// cmdUsage prints the usage.
func cmdUsage(errno int) {
	fmt.Fprintf(os.Stderr, "Usage: flador [Options]\nOptions:\n")
	flag.PrintDefaults()
	os.Exit(errno)
}

func main() {
	defer func() {
		if err := recover(); err != nil {
			fmt.Fprintf(os.Stderr, "ERROR: %s\n", err.(error).Error())
			os.Exit(1)
		}
	}()

	cwd, _ := os.Getwd()
	fVersion := flag.Bool("v", false,
		"Show version and exit.")
	fName := flag.String("i", "",
		"Input archive file.")
	fDir := flag.String("d", cwd,
		"Extract location.")
	fExt := flag.String("e", "",
		"Comma-separated selected suffix.")
	fOne := flag.Bool("1", false,
		"Extract only one file.")
	fOut := flag.String("o", "",
		"Output file, for one-file extraction only.")
	flag.Parse()

	if *fVersion {
		cmdVersion()
	}

	if *fName == "" {
		cmdUsage(1)
	}

	exts := []string{}
	for _, ext := range strings.Split(*fExt, ",") {
		ext = strings.TrimSpace(ext)
		if ext == "" {
			continue
		}
		exts = append(exts, ext)
	}

	f := fld.NewFlador(*fName, exts, *fDir)
	if !*fOne {
		if err := f.Extract(); err != nil {
			panic(err)
		}
		os.Exit(0)
	}

	if *fOut == "" {
		panic(fmt.Errorf("no output file specified"))
	}
	if err := f.ExtractOneToFile(*fOut); err != nil {
		panic(err)
	}
	os.Exit(0)
}
