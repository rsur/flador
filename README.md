# flador

general purpose archive extractor

----

![Build Status](https://ci.bfi.io/bst/build/reuzi--flador)
![Code Coverage](https://ci.bfi.io/bst/coverage/reuzi--flador)

Supported formats: `tar`, `gz`, `bz2`, `xz`, `zip`, `rar`.

Planned: `7z`.

### TODOs

- Restore attributes.
